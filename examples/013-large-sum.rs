fn load_data() -> Vec<u64> {
    use std::fs::read_to_string;
    let contents = std::fs::read_to_string("data/013-large-sum").expect("could not load data file. error");
    contents.split_whitespace().map(|line| {
        // We only care about the first 10 digits.
        // For 50 numbers, which we know there are,
        // taking the next 5 digits will give us enough accuracy.
        u64::from_str_radix(&line[0..15], 10).expect("couldn't parse input value's initial digits. error")
    }).collect()
}
fn main() {
    println!("{}", load_data().into_iter().sum::<u64>())
}